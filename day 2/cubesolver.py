import re
with open('input.txt', 'r') as file:
    total=0
    blue_limit=14
    red_limit=12
    green_limit=13
    for line in file:
        id=1
        pattern = re.compile(r'Game (\d+)')
        matches = pattern.findall(line)
        game_Id = [int(match) for match in matches]
        find = re.compile(r'(\d+) blue')
        blues = find.findall(line)
        for blue in blues: 
            if( int(blue) > int(blue_limit)):
                id= 0         
        find = re.compile(r'(\d+) red')
        reds = find.findall(line)
        for red in reds: 
            if( int(red) > int(red_limit)):
                id= 0
        find= re.compile(r'(\d+) green')
        greens = find.findall(line)
        for green in greens: 
            if( int(green) > int(green_limit)):
                id= 0        
        if (id==1):
            total = total + game_Id[0]
print(total)