import re
with open('input.txt', 'r') as file:
    total=0
    for line in file:
        id=1
        blue_limit=0
        red_limit=0
        green_limit=0
        pattern = re.compile(r'Game (\d+)')
        matches = pattern.findall(line)
        game_Id = [int(match) for match in matches]
        find = re.compile(r'(\d+) blue')
        blues = find.findall(line)
        for blue in blues: 
            if( int(blue) > int(blue_limit)):
                blue_limit=blue
        print(blue_limit)                
        find = re.compile(r'(\d+) red')
        reds = find.findall(line)
        for red in reds: 
            if( int(red) > int(red_limit)):
                red_limit=red 
        find= re.compile(r'(\d+) green')
        greens = find.findall(line)
        for green in greens: 
            if( int(green) > int(green_limit)):
                green_limit=green        
        powercube=int(green_limit)*int(red_limit)*int(blue_limit)
        total = total + powercube
print(total)