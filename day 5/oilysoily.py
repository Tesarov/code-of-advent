from array import array
import re
import time

start_time = time.time()
result=[]
checker=0
def has_letters(text):

    pattern = re.compile(r'[a-zA-Z]')

    if pattern.search(text):
        return True
    else:
        return False
    
def has_numbers(text):    
    pattern = re.compile(r'[0-9]')

    if pattern.search(text):
        return True
    else:
        return False

with open('input.txt', 'r') as file:
    first_line = file.readline()
    numbers = [int(num) for num in first_line.split()]
    print(numbers)

    for a in numbers:
        print(a)
        changed=a
        with open('input.txt', 'r') as file:
            for line in file:
                if (has_letters(line)):
                    checker=1
                    tracker=1
                    continue
                if (checker==1 and has_numbers(line) and tracker==1):
                    converter = [int(num) for num in line.split()]
                    print(converter)
                    if (converter[1]<=changed and converter[1]+converter[2]>=changed):
                        print("yay")
                        changed= converter[0] + changed - converter[1]
                        tracker=0
                        print(changed)
            result.append(changed) 
            checker=0  
    print(result)     
    print(min(result))
    end_time = time.time()
elapsed_time = end_time - start_time

print("Elapsed time:", elapsed_time, "seconds")